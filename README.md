# 简单配置即可实现直播

#### 介绍
本库，可以让开发者通过简单配置就可以实现直播功能

#### 软件架构
1.直播服务器采用nginx_rtmpdump 【[nginx](https://github.com/nginx/nginx)】【 [rtmpdump ](https://github.com/aajanki/rtmpdump)】

2.直播推流端采用 OBS 【[下载地址](https://onlinedown.rbread04.cn/huajunsafe/OBSStudio26.0.2.rar)】


#### 服务端安装及启动教程

1.  修改 html\LiveWeb\index.html 的文件 将localhost 替换成你服务器的IP地址 修改位置如下图
![输入图片说明](https://images.gitee.com/uploads/images/2020/1126/185622_0027f805_5633943.png "屏幕截图.png")

2.  双击打开启动.bat 【注意：不要关闭CMD命令窗口】

3.  浏览器中输入` http://你服务器的ip地址:8080/ ` 出现下图 直播服务端启动成功

![输入图片说明](https://images.gitee.com/uploads/images/2020/1126/185412_3ec53842_5633943.png "屏幕截图.png")

3.  浏览器中输入`http://你服务器的ip地址:8080/LiveWeb/index.html ` 出现下图 直播拉流端启动成功

![输入图片说明](https://images.gitee.com/uploads/images/2020/1126/185837_41faae6e_5633943.png "屏幕截图.png")

#### 开始直播配置说明
1.  安装好obs后在设置->推流 中填入如图对应内容，并保存 如下图

![输入图片说明](https://images.gitee.com/uploads/images/2020/1126/190621_8d23d4c0_5633943.png "屏幕截图.png")
2. 点击开始直播 即可

![输入图片说明](https://images.gitee.com/uploads/images/2020/1126/190652_b2bdc1b4_5633943.png "屏幕截图.png")

3.  浏览器中输入`http://你服务器的ip地址:8080/LiveWeb/index.html ` 开始观看直播吧

![输入图片说明](https://images.gitee.com/uploads/images/2020/1126/190927_eb7ee753_5633943.png "屏幕截图.png")

#### 使用说明

1.  先开直播服务端
2.  然后在开obs推流
3.  最后通过网站观看 

#### 注意事项

1.  建议使用win10系统做直播服务端（win7系统可能会缺少链接库）
2.  直播上限人数取决于上行网络带宽，建议千兆网络或者光纤直通服务器
3.  直播画质与最大上限人数成反比，自行合理设置 【查看obs设置[输入链接说明](https://tieba.baidu.com/p/5152111391?red_tag=0995155286)】

#### 相关内容

1.  直播异常说明文档 【[进入](https://gitee.com/jiangchunling/err_live)】
2.  通过安卓手机进行推流 【[进入](https://gitee.com/jiangchunling/ling_push)】
3.  通过安卓手机观看直播 【[进入](https://gitee.com/jiangchunling/ling_push_player)】


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
